import {Entity} from './entity';

export interface Presentation extends Entity {
  images: string[];
  counter: number;
}
