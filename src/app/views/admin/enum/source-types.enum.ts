export enum SourceTypes {
  IMAGE = 'IMAGES_PREVIEW',
  RADIO = 'RADIO_STREAMS'
}
