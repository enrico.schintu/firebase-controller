import {Component} from '@angular/core';
import {SourceTypes} from './enum/source-types.enum';
import {AngularFireDatabase} from '@angular/fire/database';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  currentSourceType: string;

  constructor(private db: AngularFireDatabase) {
  }

  handleSourceTypeSelection(sourceType: string): void {
    if (!Object.values(SourceTypes).includes(sourceType)) {
      throw new Error('Invalid source type');
    }
    this.currentSourceType = sourceType;
    this.db.object('configuration/currentSourceType').set(this.currentSourceType);
  }

}
