import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Presentation} from '../../model/presentation';
import {ImageUtils} from '../../utils/image-utils';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  presentation: Presentation;
  sourceType: any;
  backgroundImageUrl: string = null;

  private intervalTimer: any = null;

  constructor(private db: AngularFireDatabase) {
    this.db.object<Presentation>('presentation')
      .valueChanges()
      .subscribe(res => {
        this.presentation = res;
        this.playBackgroundPreview();
      });

    this.db.object<any>('configuration')
      .valueChanges()
      .subscribe(res => {
        this.sourceType = res.currentSourceType;
      });
  }

  ngOnInit(): void {
  }

  private playBackgroundPreview(): void {
    const imagesCount = this.presentation.images.length;
    if (imagesCount > 0) {
      this.backgroundImageUrl = this.presentation.images[0];
      let count = 1;
      clearInterval(this.intervalTimer);
      this.intervalTimer = setInterval(() => {
        count = count > imagesCount ? 0 : count;
        this.backgroundImageUrl = this.presentation.images[count];
        count++;
      }, 10000);
    }
  }
}
