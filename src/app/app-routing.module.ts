import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './views/admin/admin.component';
import {ViewerComponent} from './views/viewer/viewer.component';
import {MainComponent} from './main/main.component';

const routes: Routes = [
  {path: 'admin', component: AdminComponent},
  {path: 'client', component: ViewerComponent},
  {path: '**', component: MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
