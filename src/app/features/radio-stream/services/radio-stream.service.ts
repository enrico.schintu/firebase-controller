import {Injectable} from '@angular/core';
import {RadioStream, RadioStreamGroup} from '../models/radio-stream';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RadioStreamService {

  constructor(private db: AngularFireDatabase) {
    this.initializeDatabase();
  }

  public getRadioStreams(): Observable<RadioStreamGroup[]> {
    return this.db.object<RadioStreamGroup[]>('client/radio/list')
      .valueChanges();
  }

  private initializeDatabase(): Array<RadioStreamGroup> {
    const rtl: RadioStreamGroup = {
      label: 'Radio RTL', list: [
        new RadioStream(0, 'Radio RTL', 'http://shoutcast.rtl.it:3010/;stream.nsv&type=mp3'),
        new RadioStream(0, 'Radio RTL Best', 'http://shoutcast.rtl.it:3020/;stream.nsv&type=mp3'),
        new RadioStream(0, 'Radio RTL ItalianStyle', 'http://shoutcast.rtl.it:3030/;stream.nsv&type=mp3'),
        new RadioStream(0, 'Radio RTL Groove', 'http://shoutcast.rtl.it:3040/;stream.nsv&type=mp3'),
        new RadioStream(0, 'Radio RTL Lounge', 'http://shoutcast.rtl.it:3070/;stream.nsv&type=mp3'),
        new RadioStream(0, 'Radio RTL Radiofreccia', 'http://shoutcast.rtl.it:3060/;stream.nsv&type=mp3')
      ]
    };

    const kissKiss: RadioStreamGroup = {
      label: 'Radio Kiss Kiss',
      list: [new RadioStream(0, 'Radio Kiss Kiss', 'http://ice07.fluidstream.net:8080/KissKiss.mp3')]
    };

    const rai: RadioStreamGroup = {
      label: 'Rai Radio',
      list: [
        new RadioStream(0, 'Rai Radio 1', 'http://icestreaming.rai.it/1.mp3'),
        new RadioStream(0, 'Rai Radio 2', 'http://icestreaming.rai.it/2.mp3'),
        new RadioStream(0, 'Rai Radio 3', 'http://icestreaming.rai.it/3.mp3'),
        new RadioStream(0, 'Rai Radio 4 Light', 'http://icestreaming.rai.it/4.mp3'),
        new RadioStream(0, 'Rai Radio 5 Classica', 'http://icestreaming.rai.it/5.mp3'),
        new RadioStream(0, 'Rai Radio 6 Teca', 'http://icestreaming.rai.it:80/9.mp3'),
        new RadioStream(0, 'Rai Radio 7 Live', 'http://icestreaming.rai.it:80/10.mp3'),
        new RadioStream(0, 'Rai Radio 8 Opera', 'http://icestreaming.rai.it:80/11.mp3'),
        new RadioStream(0, 'Rai Radio IsoRadio', 'http://icestreaming.rai.it/6.mp3'),
        new RadioStream(0, 'Rai Radio GrParlamento', 'http://icestreaming.rai.it/7.mp3'),
      ]
    };

    const radioMonteCarlo: RadioStreamGroup = {
      label: 'Radio Monte Carlo',
      list: [
        new RadioStream(0, 'Radio Monte Carlo', 'http://edge.radiomontecarlo.net/RMC.mp3'),
      ]
    };

    const virginRadio: RadioStreamGroup = {
      label: 'Virgin Radio',
      list: [
        new RadioStream(0, 'Virgin Radio', 'http://icecast.unitedradio.it/Virgin.mp3'),
      ]
    };

    const result = [rtl, kissKiss, rai, radioMonteCarlo, virginRadio];
    this.db.object('client/radio/list').set(result);
    return result;
  }
}
