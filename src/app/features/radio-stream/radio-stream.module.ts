import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RadioStreamComponent} from './radio-stream/radio-stream.component';
import {RadioStreamService} from './services/radio-stream.service';

@NgModule({
  declarations: [
    RadioStreamComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RadioStreamComponent
  ],
  providers: [RadioStreamService]
})
export class RadioStreamModule {
}
