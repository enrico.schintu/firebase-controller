import {Entity} from '../../../model/entity';

export class RadioStream implements Entity {
  id: number;
  type: string;
  title: string;
  url: string;
  description: string;

  constructor(id: number, title: string, url: string, description?: string) {
    this.id = id;
    this.type = 'RADIO_STREAM';
    this.title = title;
    this.url = url;
    this.description = description ? description : title;
  }
}

export interface RadioStreamGroup {
  label: string;
  list: Array<RadioStream>;
}
