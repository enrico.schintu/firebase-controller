import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RadioStreamService} from '../services/radio-stream.service';
import {RadioStream, RadioStreamGroup} from '../models/radio-stream';
import {AngularFireDatabase} from '@angular/fire/database';

@Component({
  selector: 'app-radio-stream',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './radio-stream.component.html',
  styleUrls: ['./radio-stream.component.scss']
})
export class RadioStreamComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('radioPlayer') audioPlayerRef: ElementRef;

  @Input() isAdmin: boolean;

  @Input() currentRadio: RadioStream = null;

  public streamList: Array<RadioStreamGroup> = [];

  private audioPlayer: HTMLAudioElement = null;

  private subscriptions: Array<any> = [];

  constructor(private radioStreamService: RadioStreamService,
              private db: AngularFireDatabase) {
    this.subscriptions.push(this.db.object<RadioStream>('client/radio/currentRadio')
      .valueChanges()
      .subscribe(res => this.playRadio(res)));
  }

  ngOnInit() {
    this.radioStreamService.getRadioStreams().subscribe(list => this.streamList = list);
  }

  ngAfterViewInit(): void {
    if (this.currentRadio) {
      setTimeout(() => this.playRadio(), 0);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    if (this.audioPlayer) {
      this.audioPlayer.pause();
    }
  }

  setCurrentRadio(selectedRadio: RadioStream) {
    this.db.object('client/radio/currentRadio').set(selectedRadio);
    this.playRadio();
  }

  private playRadio(selectedRadio: RadioStream = this.currentRadio): void {
    this.currentRadio = selectedRadio;
    this.audioPlayer = this.audioPlayerRef.nativeElement;
    this.audioPlayer.load();
    setTimeout(() => this.audioPlayer.play(), 0);
  }

  pauseRadio() {
    this.audioPlayerRef.nativeElement.pause();
  }

}
