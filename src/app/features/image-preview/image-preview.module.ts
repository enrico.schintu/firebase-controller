import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImagePreviewComponent} from './image-preview/image-preview.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [ImagePreviewComponent],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [ImagePreviewComponent]
})
export class ImagePreviewModule {
}
