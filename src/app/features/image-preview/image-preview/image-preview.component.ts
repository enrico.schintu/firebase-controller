import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Presentation} from '../../../model/presentation';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit {

  items: string[];
  counter: number;

  constructor(private db: AngularFireDatabase) {
    db.object<Presentation>('presentation')
      .valueChanges()
      .subscribe(res => {
        this.items = (res && res.images) || [];
        this.counter = (res && res.counter) || 0;
      });
  }

  ngOnInit() {
  }

  gotoPrevImage() {
    const total = this.items.length - 1;
    const counter = (this.counter > 0) ? this.counter - 1 : total;
    this.updateCounter(counter);
  }

  gotoNextImage() {
    const total = this.items.length - 1;
    const counter = (this.counter < total) ? this.counter + 1 : 0;
    this.updateCounter(counter);
  }

  setCurrentImage(image: string) {
    const indexSelectImage = this.items.findIndex(i => i === image);
    this.updateCounter(indexSelectImage);
  }

  updateCounter(counter: number) {
    const itemRef = this.db.object('presentation/counter');
    itemRef.set(counter);
  }

  addImage(image) {
    this.items = [...this.items, image];
    this.db.object('presentation/images').set(this.items);

  }

  deleteImage(itemToDelete: string) {
    this.items = this.items.filter(item => item !== itemToDelete);
    this.db.object('presentation/images').set(this.items);
    this.updateCounter(0);
  }

  generateRandomImage() {
    const random = Math.floor(Math.random() * 200);
    const tmb = `https://picsum.photos/600/400?image=${random}`;
    this.addImage(tmb);
  }

}
