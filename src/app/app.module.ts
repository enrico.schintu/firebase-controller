import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AdminComponent} from './views/admin/admin.component';
import {ViewerComponent} from './views/viewer/viewer.component';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RadioStreamModule} from './features/radio-stream/radio-stream.module';
import {ImagePreviewModule} from './features/image-preview/image-preview.module';
import {MainComponent} from './main/main.component';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    ViewerComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    SharedModule,
    ImagePreviewModule,
    RadioStreamModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
