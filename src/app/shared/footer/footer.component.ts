import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

/**
 * @author: Enrico Schintu
 * @version: Portfolio for Angular 7
 */
@Component({
  selector: 'app-footer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnChanges {

  /** Override the default background color */
  @Input() bgColor: string = '#4b4b58';

  @Input() year: number;

  constructor(private cd: ChangeDetectorRef) {
    this.year = new Date().getFullYear();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.bgColor) {
      this.cd.markForCheck();
    }
  }
}
