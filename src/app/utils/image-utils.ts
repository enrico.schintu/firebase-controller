export class ImageUtils {

  public static generateRandomImageUrl(): string {
    const random = Math.floor(Math.random() * 200);
    return `https://picsum.photos/800/600?image=${random}`;
  }
}
