export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCJC5LX52zcoj6otTZHzta1xkfPeljYm8w',
    authDomain: 'fb-demo-c4b2c.firebaseapp.com',
    databaseURL: 'https://fb-demo-c4b2c.firebaseio.com',
    projectId: 'fb-demo-c4b2c',
    storageBucket: 'fb-demo-c4b2c.appspot.com',
    messagingSenderId: '857797392500'
  }
};
