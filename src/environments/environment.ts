// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCJC5LX52zcoj6otTZHzta1xkfPeljYm8w',
    authDomain: 'fb-demo-c4b2c.firebaseapp.com',
    databaseURL: 'https://fb-demo-c4b2c.firebaseio.com',
    projectId: 'fb-demo-c4b2c',
    storageBucket: 'fb-demo-c4b2c.appspot.com',
    messagingSenderId: '857797392500'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
